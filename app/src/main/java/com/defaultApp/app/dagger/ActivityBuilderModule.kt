package com.defaultApp.app.dagger

import com.defaultApp.app.ui.DaftarPokemon.DaftarPokemonActivity
import com.defaultApp.app.ui.DetailPokemon.DetailPokemonActivity
import com.defaultApp.app.ui.MyPokemon.MyPokemonActivity
import com.defaultApp.app.ui.listmenu.ListMenuActivity
import com.defaultApp.app.ui.login.LoginActivity
import com.defaultApp.app.ui.splashscreen.SplashScreenActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
internal abstract class ActivityBuildersModule {
    @ContributesAndroidInjector
    internal abstract fun bindSplashScreenActivity(): SplashScreenActivity

    @ContributesAndroidInjector
    internal abstract fun bindLoginActivity(): LoginActivity

    @ContributesAndroidInjector
    internal abstract fun DaftarPokemonActivity(): DaftarPokemonActivity

    @ContributesAndroidInjector
    internal abstract fun DetailPokemonActivity(): DetailPokemonActivity

    @ContributesAndroidInjector
    internal abstract fun MyPokemonActivity(): MyPokemonActivity

    @ContributesAndroidInjector
    internal abstract fun ListMenuActivity(): ListMenuActivity
}