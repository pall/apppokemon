package com.defaultApp.app.dagger

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.defaultApp.app.ui.DaftarPokemon.DaftarPokemonViewModel
import com.defaultApp.app.ui.DetailPokemon.DetailPokemonViewModel
import com.defaultApp.app.ui.login.LoginViewModel
import com.defaultApp.app.ui.splashscreen.SplashScreenViewModel
import com.defaultApp.app.util.AppVMFactory
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Suppress("unused")
@Module
abstract class ViewModelModule {
    @Binds
    abstract fun bindViewModelFactory(factory: AppVMFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(SplashScreenViewModel::class)
    abstract fun bindSplashScreenViewModel(viewModel: SplashScreenViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(LoginViewModel::class)
    abstract fun bindLoginViewModel(viewModel: LoginViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(DaftarPokemonViewModel::class)
    abstract fun bindDaftarPokemonViewModel(viewModel: DaftarPokemonViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(DetailPokemonViewModel::class)
    abstract fun bindDetailPokemonViewModel(viewModel: DetailPokemonViewModel): ViewModel

}