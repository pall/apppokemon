package com.defaultApp.app

import com.defaultApp.app.api.model.RequestHeader
import com.defaultApp.app.dagger.DaggerAppComponent
import com.defaultApp.app.repository.UserRepository
import com.defaultApp.app.util.SchedulerProvider
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication
import javax.inject.Inject

class DefaultApp : DaggerApplication() {
    @Inject
    lateinit var requestHeader: RequestHeader
    @Inject
    lateinit var userRepository: UserRepository

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerAppComponent.builder().create(this)
    }

    override fun onCreate() {
        super.onCreate()
        initializeApplication()
    }

    private fun initializeApplication() {
        //load the current user into the system
        val user =
            userRepository.getLocalUserData().subscribeOn(SchedulerProvider.instance.computation())
                ?.blockingGet()

        //load the current access token into all requests
//        if (user != null) {
//            requestHeader.accesstoken.accessToken = user.accessToken.toString()
//            requestHeader.employeeId = user.id.toString()
//        }

    }

}