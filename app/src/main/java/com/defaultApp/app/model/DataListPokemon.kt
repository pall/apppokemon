package com.defaultApp.app.model

data class DataListPokemon(
    val count: Int,
    val next: String,
    val previous: Any,
    val results: List<ResultList>
)

data class ResultList(
    val name: String,
    val url: String,
    val nickName: String? = null,
    val idDB: Int? = 0
) {
    val id: Int
        get() = url.split("/".toRegex()).dropLast(1).last().toInt()

    val imageUrl: String
        get() = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/$id.png"
}