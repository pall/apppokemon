package com.defaultApp.app.util

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.defaultApp.app.R

object BindingAdapter {
    @JvmStatic
    @BindingAdapter("addSrc")
    fun addSrc(view: ImageView, show: Int) {
        view.setBackgroundResource(show)
    }

    @BindingAdapter("addGlideImage")
    @JvmStatic
    fun addGlideImage(view: ImageView, txt: String?) {
        GlideApp.with(view.context)
            .load(txt)
            .placeholder(R.drawable.animation_progress)
            .override(150, 150)
            .error(R.drawable.ic_account)
            .into(view)
    }

}