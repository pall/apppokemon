package com.defaultApp.app.ui.DetailPokemon

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.defaultApp.app.cache.PreferencesHelper
import com.defaultApp.app.model.DataDetailPokemon
import com.defaultApp.app.repository.UserRepository
import com.defaultApp.app.ui.base.BaseViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

class DetailPokemonViewModel @Inject constructor(
    val userRepository: UserRepository,
    val preferencesHelper: PreferencesHelper
) : BaseViewModel() {
    private val datas = MutableLiveData<DataDetailPokemon>()
    val detailPokemon: LiveData<DataDetailPokemon> get() = datas
    var name: String = ""
    var imgUrl: String = ""
    var url: String = ""
    var state: Boolean = false
    var nickname: String = ""
    var idDB: Int? = 0

    fun init() {
        viewModelScope.launch {
            datas.value = userRepository.getDetailPokemon(name)
        }
    }
}