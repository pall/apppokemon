package com.defaultApp.app.ui.DetailPokemon

import android.app.AlertDialog
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.room.Room
import com.bumptech.glide.Glide
import com.defaultApp.app.Constants
import com.defaultApp.app.R
import com.defaultApp.app.dataDB.MyPokemon
import com.defaultApp.app.dataDB.PokemonDB
import com.defaultApp.app.databinding.ActivityDetailPokemonBinding
import com.defaultApp.app.ui.base.BaseActivity
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import javax.inject.Inject

class DetailPokemonActivity: BaseActivity() {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var viewModel: DetailPokemonViewModel
    private lateinit var binding: ActivityDetailPokemonBinding
    lateinit var db: PokemonDB


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_detail_pokemon)
        viewModel = initViewModel(DetailPokemonViewModel::class.java, viewModelFactory)
        viewModel.name = intent.getStringExtra("name").toString()
        viewModel.imgUrl = intent.getStringExtra("imgUrl").toString()
        viewModel.url = intent.getStringExtra("url").toString()
        viewModel.state = intent.getBooleanExtra("state",false)
        viewModel.nickname = intent.getStringExtra("nickname").toString()
        if (viewModel.state) {
            setToolbar(viewModel.name)
        } else {
            setToolbar(viewModel.nickname)
            viewModel.idDB = intent.getIntExtra("idDB",0)
        }

        viewModel.init()
        initData()
        initObserver()
        initListener()
    }

    private fun initData() {
        db = Room.databaseBuilder(
            applicationContext,
            PokemonDB::class.java, "PokemonDB"
        ).build()
    }


    private fun initObserver() {
        viewModel.detailPokemon.observe(this, Observer {
            if (it != null) {
                Log.d("Debug-pak","data :"+viewModel.imgUrl)
                Glide.with(this)
                    .load(viewModel.imgUrl)
                    .placeholder(R.drawable.no_img)
                    // .diskCacheStrategy(DiskCacheStrategy.NONE)
                    // .skipMemoryCache(true)
                    .error(R.drawable.no_img)
                    .into(binding.imgContent)
                binding.valueTypes.text = it.types.joinToString { it.type.name }
                binding.valueMoves.text = it.moves.joinToString { it.move.name }
            }
        })
    }

    private fun initListener() {
        if (viewModel.state) {
            binding.btnCatch.visibility = View.VISIBLE
            binding.layoutBtn.visibility = View.GONE
        } else {
            binding.btnCatch.visibility = View.GONE
            binding.layoutBtn.visibility = View.VISIBLE
        }
        binding.btnCatch.setOnClickListener {
            if (Math.random() < 0.5) {
                val builder = AlertDialog.Builder(this)
                builder.setTitle("Give your Pokemon a nickname:")
                val nicknameEditText = EditText(this)
                builder.setView(nicknameEditText)
                builder.setPositiveButton("OK") { dialog, _ ->
                    // Do nothing here, we override this button later
                }
                builder.setNegativeButton("Cancel") { dialog, _ ->
                    dialog.cancel()
                }
                val dialog = builder.create()
                dialog.setOnShowListener {
                    val button = dialog.getButton(AlertDialog.BUTTON_POSITIVE)
                    button.setOnClickListener {
                        val text = nicknameEditText.text.toString()
                        if (text.isEmpty()) {
                            Toast.makeText(this, "Nickname cannot be empty", Toast.LENGTH_SHORT).show()
                        } else {
                            val nickname = nicknameEditText.text.toString()
                            viewModel.nickname = nickname
                            addToMyPokemonList(viewModel.name, nickname, viewModel.url)
                            dialog.dismiss()
                        }
                    }
                }
                dialog.show()
            } else {
                Toast.makeText(this, "Failed to catch Pokemon", Toast.LENGTH_SHORT).show()
            }
        }

        binding.btnRelease.setOnClickListener {
            GlobalScope.launch {
                db.pokemonDao().releasePokemon(viewModel.idDB ?: 0)
            }
            binding.btnCatch.visibility = View.VISIBLE
            binding.layoutBtn.visibility = View.GONE
            Toast.makeText(this, "Success", Toast.LENGTH_SHORT).show()
        }
        binding.btnRename.setOnClickListener {
            val builder = AlertDialog.Builder(this)
            builder.setTitle("Rename your Pokemon :")
            val nicknameEditText = EditText(this)
            nicknameEditText.setText(viewModel.nickname)
            builder.setView(nicknameEditText)
            builder.setPositiveButton("OK") { dialog, _ ->
                // Do nothing here, we override this button later
            }
            builder.setNegativeButton("Cancel") { dialog, _ ->
                dialog.cancel()
            }
            val dialog = builder.create()
            dialog.setOnShowListener {
                val button = dialog.getButton(AlertDialog.BUTTON_POSITIVE)
                button.setOnClickListener {
                    val text = nicknameEditText.text.toString()
                    if (text.isEmpty()) {
                        Toast.makeText(this, "Nickname cannot be empty", Toast.LENGTH_SHORT).show()
                    } else {
                        val nickname = nicknameEditText.text.toString()
                        updateMyPokemon(viewModel.name, nickname, viewModel.url, viewModel.idDB ?: 0)
                        dialog.dismiss()
                    }
                }
            }
            dialog.show()
        }
    }

    private fun addToMyPokemonList (name: String, nickName: String, imgUrl: String) {
        GlobalScope.launch {
            val datas = MyPokemon (
                name,
                imgUrl,
                nickName
            )
            db.pokemonDao().insertAll(datas)
        }
        binding.btnCatch.visibility = View.GONE
        binding.layoutBtn.visibility = View.VISIBLE
        Toast.makeText(this, "Successfully caught Pokemon", Toast.LENGTH_SHORT).show()
    }

    private fun updateMyPokemon (name: String, nickName: String, imgUrl: String, id: Int) {
        GlobalScope.launch {
            db.pokemonDao().updateDataById(id,name,imgUrl,nickName)
        }

        Toast.makeText(this, "Successfully update", Toast.LENGTH_SHORT).show()
    }
}