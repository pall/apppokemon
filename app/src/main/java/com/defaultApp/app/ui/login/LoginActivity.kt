package com.defaultApp.app.ui.login

import android.os.Build
import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.defaultApp.app.R
import com.defaultApp.app.databinding.ActivityLoginBinding
import com.defaultApp.app.ui.base.BaseActivity
import javax.inject.Inject

class LoginActivity : BaseActivity() {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var viewModel: LoginViewModel

    private lateinit var binding: ActivityLoginBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login)
        viewModel = initViewModel(LoginViewModel::class.java, viewModelFactory)
        binding.viewModel = viewModel

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.statusBarColor = ContextCompat.getColor(this, R.color.colorPrimary)
        }

//        initObserver()
//        initListener()
    }
/*
    private fun initListener() {
        binding.etPass.setOnKeyListener { v, keyCode, event ->
            when (keyCode) {
                KeyEvent.KEYCODE_ENTER -> loginCheck()
            }
            false
        }
        binding.btLogin.setOnClickListener {
            loginCheck()
        }
        binding.tvForget.setOnClickListener {
            binding.layoutLogin.visibility = View.GONE
            binding.layoutForgetPassword.visibility = View.VISIBLE
        }
        binding.btnBackToLogin.setOnClickListener {
            binding.layoutLogin.visibility = View.VISIBLE
            binding.layoutForgetPassword.visibility = View.GONE
        }
    }

    private fun loginCheck() {
        if (!binding.etUsername.text.isNullOrEmpty() || !binding.etPass.text.isNullOrEmpty()) {
            viewModel.username.set(binding.etUsername.text.toString())
            viewModel.pass.set(binding.etPass.text.toString())
            viewModel.login()
        } else Toast.makeText(
            this,
            "Email dan Password tidak boleh kosong",
            Toast.LENGTH_SHORT
        ).show()
    }

    private fun initObserver() {
        viewModel.check.observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    Log.d("debug-login","data :"+it.data)
                    if (it.data != null) {
                        startActivity(Intent(this, MainActivity::class.java))
                        finish()
                    }
                }
                Status.VALIDATION_ERROR -> {
                    Toast.makeText(this, "Email / Password salah", Toast.LENGTH_SHORT).show()
                }
                Status.ERROR -> {
                    Toast.makeText(this, "Terjadi kesalahan", Toast.LENGTH_SHORT).show()
                }
            }
        })
    }*/
}