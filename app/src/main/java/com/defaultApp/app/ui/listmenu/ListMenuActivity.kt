package com.defaultApp.app.ui.listmenu

import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.defaultApp.app.R
import com.defaultApp.app.databinding.ActivityListMenuBinding
import com.defaultApp.app.ui.DaftarPokemon.DaftarPokemonActivity
import com.defaultApp.app.ui.MyPokemon.MyPokemonActivity
import com.defaultApp.app.ui.base.BaseActivity

class ListMenuActivity: BaseActivity() {
    private lateinit var binding: ActivityListMenuBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_list_menu)
        setToolbar("Menu")
        initListener()
    }

    private fun initListener() {
        binding.menuListPokomenon.setOnClickListener {
            startActivity(Intent(this, DaftarPokemonActivity::class.java))
        }
        binding.menuMyPokemon.setOnClickListener {
            startActivity(Intent(this, MyPokemonActivity::class.java))
        }
    }
}