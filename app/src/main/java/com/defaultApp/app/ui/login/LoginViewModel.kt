package com.defaultApp.app.ui.login

import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.defaultApp.app.api.model.Resource
import com.defaultApp.app.cache.PreferencesHelper
import com.defaultApp.app.repository.UserRepository
import com.defaultApp.app.ui.base.BaseViewModel
import javax.inject.Inject

class LoginViewModel @Inject constructor(
    val userRepository: UserRepository,
    val preferencesHelper: PreferencesHelper
) : BaseViewModel() {
    var name = ObservableField("")
    var username = ObservableField("")
    var pass = ObservableField("")
    var checkTrigger: MutableLiveData<Boolean> = MutableLiveData()

    /*var check: LiveData<Resource<LoginData>> = Transformations.switchMap(checkTrigger) {
        userRepository.login(username.get()!!, pass.get()!!)
    }*/

    fun login() {
        checkTrigger.value = checkTrigger.value == false
    }

}