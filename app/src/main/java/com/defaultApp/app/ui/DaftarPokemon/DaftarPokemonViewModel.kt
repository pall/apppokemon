package com.defaultApp.app.ui.DaftarPokemon

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.defaultApp.app.cache.PreferencesHelper
import com.defaultApp.app.model.DataListPokemon
import com.defaultApp.app.repository.UserRepository
import com.defaultApp.app.ui.base.BaseViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

class DaftarPokemonViewModel @Inject constructor(
    val userRepository: UserRepository,
    val preferencesHelper: PreferencesHelper
) : BaseViewModel() {
    private val listPokemon = MutableLiveData<DataListPokemon>()
    val dataPokemon: LiveData<DataListPokemon> get() = listPokemon
    var page: Int = 0

    fun init() {
        viewModelScope.launch {
            listPokemon.value = userRepository.getListPokemon(page, 20)
        }
    }
}