package com.defaultApp.app.ui.DaftarPokemon

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.defaultApp.app.R
import com.defaultApp.app.databinding.ActivityDaftarPokemonBinding
import com.defaultApp.app.ui.base.BaseActivity
import com.defaultApp.app.ui.main.DaftarPokemonAdapter
import com.defaultApp.app.util.OnLoadMoreListener
import javax.inject.Inject

class DaftarPokemonActivity: BaseActivity() {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var viewModel: DaftarPokemonViewModel
    private lateinit var binding: ActivityDaftarPokemonBinding
    lateinit var adapter: DaftarPokemonAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_daftar_pokemon)
        viewModel = initViewModel(DaftarPokemonViewModel::class.java, viewModelFactory)
        setToolbar("List Pokemon")
        initAdapter()
        viewModel.init()
        initObserver()
    }

    private fun initAdapter() {
        adapter = DaftarPokemonAdapter( this, binding.rvListPokemon)
        binding.rvListPokemon.adapter = adapter

        adapter.setOnloadMoreListener(object : OnLoadMoreListener {
            override fun onLoadMore() {
                binding.pbList.visibility = View.VISIBLE
                viewModel.init()
            }
        })

    }

    private fun initObserver() {
        viewModel.dataPokemon.observe(this, Observer {
            binding.pbList.visibility = View.GONE
            if (it != null) {
                adapter.isLoading = false
                if (viewModel.page == 0) {
                    adapter.data.clear()
                    adapter.notifyDataSetChanged()
                }
                val tmpSize = adapter.data.size
                for (i in it.results.indices){
                    adapter.data.add(it.results[i])
                    adapter.notifyItemInserted(tmpSize)
                }

                viewModel.page += 20
            }
        })
    }
}