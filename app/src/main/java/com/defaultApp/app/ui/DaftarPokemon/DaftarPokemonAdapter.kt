package com.defaultApp.app.ui.main

import android.app.Activity
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.defaultApp.app.Constants
import com.defaultApp.app.R
import com.defaultApp.app.dataDB.MyPokemon
import com.defaultApp.app.databinding.ItemListPokemonBinding
import com.defaultApp.app.model.DataListPokemon
import com.defaultApp.app.model.ResultList
import com.defaultApp.app.ui.DaftarPokemon.DaftarPokemonActivity
import com.defaultApp.app.ui.DetailPokemon.DetailPokemonActivity
import com.defaultApp.app.ui.MyPokemon.MyPokemonActivity
import com.defaultApp.app.util.OnLoadMoreListener

class DaftarPokemonAdapter (
    val activity: Activity,
    private val recyclerView: RecyclerView
): RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    var data: MutableList<ResultList> = arrayListOf()
    var lastVisibleItem = 0
    var totalItem = 0
    var isLoading = false
    var onLoadMoreListener: OnLoadMoreListener? = null
    val visibleThresHold = 10


    init {
        val linearLayoutManager = recyclerView.layoutManager as LinearLayoutManager
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)

                totalItem = linearLayoutManager.itemCount
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition()
                if (!isLoading && totalItem < lastVisibleItem + visibleThresHold && dy > 0) {
                    if (onLoadMoreListener != null) {
                        onLoadMoreListener!!.onLoadMore()
                    }
                    isLoading = true
                }
            }
        })
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ItemListPokemonHolder(
            ItemListPokemonBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is ItemListPokemonHolder -> holder.bind(data[position])
        }
    }
    inner class ItemListPokemonHolder(itemView: ItemListPokemonBinding) :
        RecyclerView.ViewHolder(itemView.root) {
        val binding = itemView
        fun bind(datas: ResultList){
            Glide.with(activity)
                .load(datas.imageUrl)
                .placeholder(R.drawable.no_img)
                // .diskCacheStrategy(DiskCacheStrategy.NONE)
                // .skipMemoryCache(true)
                .error(R.drawable.no_img)
                .into(binding.imgPokemon)

            var state = false
            if (activity.localClassName.contains("DaftarPokemonActivity")) {
                state = true
                binding.tvPokName.text = datas.name
            } else if (activity.localClassName.contains("MyPokemonActivity")) {
                state = false
                binding.tvPokName.text = datas.nickName
            }
            binding.layoutContent.setOnClickListener {
                activity.startActivity(
                    Intent(activity, DetailPokemonActivity::class.java)
                    .putExtra("url",datas.url)
                    .putExtra("name",datas.name)
                    .putExtra("nickname",datas.nickName)
                    .putExtra("imgUrl",datas.imageUrl)
                    .putExtra("state",state)
                    .putExtra("idDB",datas.idDB)
                )
            }
        }
    }

    fun setOnloadMoreListener(onLoadMoreListener: OnLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener
    }

}
