package com.defaultApp.app.ui.MyPokemon

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.room.Room
import com.defaultApp.app.R
import com.defaultApp.app.dataDB.MyPokemon
import com.defaultApp.app.dataDB.PokemonDB
import com.defaultApp.app.databinding.ActivityDetailPokemonBinding
import com.defaultApp.app.databinding.ActivityMyPokemonBinding
import com.defaultApp.app.model.ResultList
import com.defaultApp.app.ui.base.BaseActivity
import com.defaultApp.app.ui.main.DaftarPokemonAdapter
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class MyPokemonActivity: BaseActivity() {
//    @Inject
//    lateinit var viewModelFactory: ViewModelProvider.Factory
//    private lateinit var viewModel: MyPokemonViewModel
    private lateinit var binding: ActivityMyPokemonBinding
    lateinit var adapter: DaftarPokemonAdapter
    lateinit var db: PokemonDB


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_my_pokemon)
//        viewModel = initViewModel(MyPokemonViewModel::class.java, viewModelFactory)
        setToolbar("My Pokemon")
        initAdapter()
        initData()
    }

    private fun initAdapter() {
        adapter = DaftarPokemonAdapter( this, binding.rvMyPokemon)
        binding.rvMyPokemon.adapter = adapter
    }

    private fun initData() {
        db = Room.databaseBuilder(
            applicationContext,
            PokemonDB::class.java, "PokemonDB"
        ).build()

        GlobalScope.launch {
            val dataisi: List<MyPokemon> = db.pokemonDao().getListPokeDB()
            if (dataisi.isEmpty()) {
                binding.tvInfo.visibility = View.VISIBLE
            } else {
                binding.tvInfo.visibility = View.GONE
                dataisi.forEach {
                    adapter.data.add(ResultList(
                        it.name,
                        it.imgUrl,
                        it.nickname,
                        it.id
                    ))
                }
            }
        }
        adapter.notifyDataSetChanged()
    }

    private fun initObserver() {
//        viewModel.dataPokemon.observe(this, Observer {
////            binding.pbGenre.visibility = View.GONE
//            if (it != null) {
//                for (i in it.results.indices){
//                    adapter.data.add(it.results[i])
//                    adapter.notifyDataSetChanged()
//                }
//            }
//        })
    }
}
