package com.defaultApp.app.api

import com.defaultApp.app.model.*
import retrofit2.http.*


interface ApiService {
    @GET("pokemon")
    suspend fun getListpokemon(
        @Query("offset") page: Int,
        @Query("limit") limit: Int
    ): DataListPokemon

    @GET("pokemon/{name}")
    suspend fun getDetailPokemon(
        @Path("name") name: String
    ): DataDetailPokemon
}