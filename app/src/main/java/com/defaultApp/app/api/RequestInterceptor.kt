package com.defaultApp.app.api

import android.util.Log
import com.defaultApp.app.Constants
import com.defaultApp.app.api.model.RequestHeader
import okhttp3.Interceptor
import okhttp3.Response

class RequestInterceptor(private val requestHeaders: RequestHeader) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val original = chain.request()
        val builder =
            original.newBuilder()
//                .header("Authorization", "Bearer " + requestHeaders.accesstoken.accessToken)
//                .header("Authorization", "Bearer " + Constants.AccessToken)
//                .header("employee-id", requestHeaders.employeeId)
//                .header("X-AUTHENTICATED-USERID","1")
                .header("Accept", requestHeaders.language)
//                .header("Accept", "application/json")
                .header("User-Agent", "okhttp")
                .method(original.method, original.body)
        val newRequest = builder.build()
//        Log.d("debug-token", requestHeaders.accesstoken.accessToken)

        return chain.proceed(newRequest)
    }
}