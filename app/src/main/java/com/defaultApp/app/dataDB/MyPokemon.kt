package com.defaultApp.app.dataDB

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

//@Entity(tableName = "pokemon", indices = [Index(value = ["imgUrl"], unique = true)])
@Entity(tableName = "pokemon")
data class MyPokemon(
    @ColumnInfo(name = "name") val name: String,
    @ColumnInfo(name = "imgUrl") val imgUrl: String,
    @ColumnInfo(name = "nickname") val nickname: String
) {
    @PrimaryKey(autoGenerate = true)
    var id: Int? = null
}
