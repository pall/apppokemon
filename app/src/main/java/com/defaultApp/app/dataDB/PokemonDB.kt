package com.defaultApp.app.dataDB

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(
    entities = [MyPokemon::class],
    version = 1,
    exportSchema = false
)

abstract class PokemonDB: RoomDatabase() {
    abstract fun pokemonDao(): DataDAO
}