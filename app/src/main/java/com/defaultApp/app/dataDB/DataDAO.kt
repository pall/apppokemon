package com.defaultApp.app.dataDB

import androidx.room.ColumnInfo
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface DataDAO {
    //    @Insert(onConflict = OnConflictStrategy.REPLACE)
    @Insert()
    fun insertAll(vararg users: MyPokemon)

    @Query("SELECT * FROM pokemon ORDER BY id DESC")
    fun getListPokeDB(): List<MyPokemon>

    @Query("DELETE FROM pokemon WHERE id = :id")
    fun releasePokemon(id: Int)

    @Query("UPDATE pokemon SET name = :name, imgUrl = :imgUrl, nickname = :nickname WHERE id = :id")
    fun updateDataById(id: Int, name: String, imgUrl:String, nickname:String)

    /*@Query("SELECT * FROM income WHERE id = :id")
    fun getItem(id: Int): DanaDB

    @Query("DELETE FROM income WHERE id = :id")
    fun deleteItem(id: Int)

    @Query("SELECT * FROM income WHERE date = :date ORDER BY id DESC")
    fun getCurrentDate(date: String): List<DanaDB>

    @Query("UPDATE income SET date = :date, time = :time, moneyIn = :moneyIn, `from` = :from, nominal = :nominal, keterangan = :keterangan WHERE id = :id")
    fun updateDataById(id: Int, date: String, time:String, moneyIn:String, from:String, nominal:String, keterangan:String)*/
}
